/**
 * 
 */
package com.josetrigueros.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


/**
 * @author Jose Trigueros
 *
 */
public class Paper
{
    private int paperID;
    private int year;
    private double weight;
    private Keyword[] keywordList;
    
    public Paper( int paperID, int year, Keyword[] terms)
    {
        this.paperID = paperID;
        this.year = year;
        this.weight = 1.0;
        this.keywordList = (terms == null ) ? new Keyword[]{} : terms;
    }
    
    public void printTermFrequencies()
    {
        Arrays.sort(keywordList,Keyword.TFComparator);
        if ( keywordList.length != 0 )
        {
            for ( Keyword keyword : keywordList )
            {
//                System.out.println("Term: " + keyword.getKeyword());
                double freq = keyword.getFreq();
//                System.out.printf("TF: %.8f %n%n", freq);
                System.out.printf("%.8f : %s%n", freq, keyword.getKeyword());
            }
        }
        else
            System.out.println("There are no terms.");
    }
    
    public void printTFIDF()
    {
        Arrays.sort(keywordList,Keyword.TFIDFComparator);
        
        if ( keywordList.length != 0 )
        {
            for ( Keyword keyword : keywordList )
            {
//                System.out.println("Term: " + keyword.getKeyword());
//                System.out.printf("TF-IDF: %.8f %n%n",keyword.getFreq()*keyword.getInverseDocFreq());
                System.out.printf("%.8f : %s%n", keyword.getFreq()*keyword.getInverseDocFreq(), keyword.getKeyword());
            }
        }
        else
            System.out.println("There are no terms.");
    }
    
    public void printTerms()
    {
        for ( Keyword k : keywordList )
        {
            System.out.println(k.getKeyword());
        }
    }
    
    public Paper add( Paper p )
    {
        // Convert 'this' keywordList to an ArrayList so that it can be searched
        ArrayList<Keyword> addBuffer = new ArrayList<Keyword>(Arrays.asList(keywordList));
        // Iterate through the incoming papers' keywords and add them over
        for ( Keyword keyword : p.getKeywordList() )
        {
            Collections.sort(addBuffer);
            int keywordIndex = Collections.binarySearch(addBuffer, keyword);
            Keyword weightedKeyword = keyword.multiplyScalar(p.getWeight());
            if( keywordIndex < 0)
            {
                // The keyword is not in the buffer, create it and add it.
                addBuffer.add(weightedKeyword);
            }
            else
            {
                // The keyword is in the buffer, look for it and add to it.
                Keyword thisKeyword = addBuffer.get(keywordIndex);
                thisKeyword.setFreq(thisKeyword.getFreq() + weightedKeyword.getFreq());
                thisKeyword.setFreq(thisKeyword.getInverseDocFreq() + weightedKeyword.getInverseDocFreq());
            }
        }
        
        // Trim down the fat
        addBuffer.trimToSize();
        
        // Return a new array with everything accumulated
        return new Paper(-1, -1, addBuffer.toArray(new Keyword[addBuffer.size()]));
    }
    
    public Paper multiplyScalar( double scalar )
    {
        Keyword[] multBuffer = new Keyword[keywordList.length];
        
        for ( int i = 0; i < keywordList.length; i++ )
        {
            multBuffer[i] = keywordList[i].multiplyScalar(scalar);
        }
        
        return new Paper(-1,-1,multBuffer);
    }
    
    public Paper divideScalar( double scalar )
    {
        return multiplyScalar(1/scalar);
    }

    /**
     * @return the paperID
     */
    public int getPaperID()
    {
        return paperID;
    }

    /**
     * @return the year
     */
    public int getYear()
    {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear( int year )
    {
        this.year = year;
    }

    /**
     * @return the weight
     */
    public double getWeight()
    {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight( double weight )
    {
        this.weight = weight;
    }

    /**
     * @return the keywordList
     */
    public Keyword[] getKeywordList()
    {
        return keywordList;
    }

    /**
     * @param keywordList the keywordList to set
     */
    public void setKeywordList( Keyword[] keywordList )
    {
        this.keywordList = keywordList;
    }

    public boolean contains( Keyword word )
    {
        ArrayList<Keyword> list = new ArrayList<Keyword>(Arrays.asList(keywordList));
        Collections.sort(list);
        if( Collections.binarySearch(list, word) >= 0 )
            return true;
        return false;
    }

}

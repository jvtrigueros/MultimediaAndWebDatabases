package com.josetrigueros;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.josetrigueros.database.DBLP;
import com.josetrigueros.database.DatabaseConnection;
import com.josetrigueros.entities.Author;
import com.josetrigueros.entities.Corpus;
import com.josetrigueros.entities.Keyword;
import com.josetrigueros.entities.Paper;
import com.josetrigueros.lucene.LuceneHandler;
import com.josetrigueros.utilities.DeleteDirectory;

public class Driver
{
    private static final String PRINT_PAPER_VECTOR = "print_paper_vector";
    private static final String PRINT_AUTHOR_VECTOR = "print_author_vector";
    private static final String DIFF_AUTHOR = "differentiate_author";
    
    private static final String TF = "tf";
    private static final String TFIDF = "tfidf";
    private static final String PF = "pf";
    
    private static final String LUCENE_DIR = "./TestIndex";
    
    public static void main( String[] args )
    {               
        // Bail if the input is invalid
        if( args.length != 3 )
            return;
        
        // Acquire Database Connection ( This may not be needed ) 
        DatabaseConnection db = new DatabaseConnection(DBLP.PATH);
        
        // Delete Lucene index if present
        DeleteDirectory.recursive(LUCENE_DIR);
        
        // Split the arguments into variables
        String task = args[0];
        int id = Integer.parseInt(args[1]);
        String model = args[2];
        
        /**
         * Determine which query was made:
         * 1: print_paper_vector paperid model
         * 2: print_author_vector authorid model
         * 3: differentiate_author authorid model
         */
        if( task.equals(PRINT_PAPER_VECTOR) )
        {
            // Do task 1
            if ( model.equals(TF) )
                task1(db).getPaper(id).printTermFrequencies();
            else if( model.equals(TFIDF) )
                task1(db).getPaper(id).printTFIDF();
        }
        else if( task.equals(PRINT_AUTHOR_VECTOR) )
        {
            // Do task 2
            if ( model.equals(TF) )
                task2(db, id).printTermFrequencies();
            else if( model.equals(TFIDF) )
                task2(db, id).printTFIDF();
        }
        else if( task.equals(DIFF_AUTHOR) )
        {
            // Do task 3
            task3(db, id, model);
        }
        
        // Close Database Connection
        db.disconnect();
    }
    
    private static Corpus task1(DatabaseConnection db)
    {
        return new Corpus(generateFullDatabaseLuceneIndex(db));
    }

    private static Paper task2(DatabaseConnection db, int authorid)
    {
        Corpus corpus = new Corpus(generateFullDatabaseLuceneIndex(db), true);
        return generateAuthor(db, corpus, authorid).getAveragePaper();
    }
    
    private static void task3( DatabaseConnection db, int authorid, String model )
    {
        // Create another corpus that contains authors and co-authors
        Corpus self_coauthorCorpus = new Corpus(generateSelfCoAuthorLuceneIndex(db, authorid));
        Author self_coauthor = generateAuthor(db, self_coauthorCorpus, authorid);
        
        // Delete index, since I can't unlock it.
        DeleteDirectory.recursive(LUCENE_DIR);
        
        if( model.equals(TFIDF) )
        {
            // I can use this corpus to create an author
            Corpus authorCorpus = new Corpus(generateAuthorLuceneIndex(db, authorid));
            Author self = generateAuthor(db, authorCorpus, authorid);
            
            self.printTFIDF2(self_coauthor);
        }
        else if ( model.equals(PF) )
        {
            // Create a corpus just for co-authors
            Corpus coauthorCorpus = new Corpus(generateCoAuthorLuceneIndex(db, authorid));
            
            // Get list of keywords to apply PF with
            Keyword[] keywordList = self_coauthor.getAveragePaper().getKeywordList();
            
            // Constants
            double R = coauthorCorpus.getCorpusSize();
            double N = self_coauthorCorpus.getCorpusSize();
            double b = N - R;
            
            for ( Keyword keyword : keywordList )
            {
                double r = coauthorCorpus.inverseTermFreq(keyword);
                double n = self_coauthorCorpus.inverseTermFreq(keyword);
                
                // Taking PF apart and doing it in pieces
                double a = n - r;
                double c = r/(R-r);
                double d = r/R;
                
                // Computing PF
                double PF_1 = Math.log( c / ( a/(b-a) ) );
                double PF_2 = Math.abs(d-a/b);
                
                // Print PF
                System.out.println("Term: " + keyword.getKeyword());
                System.out.printf("PF: %.8f %n%n", PF_1*PF_2);
            }
        }
    }
    
    private static LuceneHandler generateFullDatabaseLuceneIndex( DatabaseConnection db )
    {
        // Create Field List and get a Lucene Instance
        String[] fieldList = {DBLP.PAPERID, DBLP.PAPERS_YEAR, DBLP.PAPERS_ABSTRACT};
        LuceneHandler luceneHandle = LuceneHandler.createLuceneInstance(LUCENE_DIR);
        
        // Query DB for something
        StringBuilder queryBuffer = new StringBuilder();
        
        // Adding Selects
        queryBuffer.append( "Select ");
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[0] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[1] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[2] + "\n" );
        
        // Adding From
        queryBuffer.append("From ");
        queryBuffer.append(DBLP.PAPERS + "\n");
        
        // Merging query
        String query = queryBuffer.toString();
        
        // Populating Lucene Index
        ResultSet rs = db.query(query);
        try
        {
            while(rs.next())
            {
                ArrayList<String> currentResult = new ArrayList<String>();
                for ( String field : fieldList )
                {
                    String value = rs.getString(field);
                    value = ( value == null ) ? "" : value;
                    currentResult.add(value);
                }
                
                // Add document with given array of fields and values
                luceneHandle.addDocument(Integer.parseInt(rs.getString(DBLP.PAPERID)), fieldList, currentResult);
            }
            luceneHandle.fixIndexList();
            rs.close();
        } catch ( NumberFormatException e )
        {
            e.printStackTrace();
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        return luceneHandle;
    }

    /**
     * Generates an Author object which contains all the papers written by authorid
     * @param db
     * @param corpus
     * @param authorid
     * @return
     */
    private static Author generateAuthor( DatabaseConnection db, Corpus corpus, int authorid )
    {
        // Form new query to get all of the papers written by an author
        StringBuilder queryBuffer = new StringBuilder();
        
        // Select 
        queryBuffer.append("Select ");
        queryBuffer.append(DBLP.PAPERID);
        queryBuffer.append("\n");
        
        // From
        queryBuffer.append("From ");
        queryBuffer.append(DBLP.WRITTENBY);
        queryBuffer.append("\n");
        
        // Where
        queryBuffer.append("Where ");
        queryBuffer.append(DBLP.PERSONID + "='" + authorid + "'");
        queryBuffer.append("\n");
        
        // Get Result Set
        ResultSet rs = db.query(queryBuffer.toString());
        ArrayList<Paper> papers = new ArrayList<Paper>();
        try
        {
            while ( rs.next() )
            {
                int paperid = Integer.parseInt(rs.getString(DBLP.PAPERID));
                Paper p = corpus.getPaper(paperid);
                if( p != null)
                    papers.add(p);
            }
        } catch ( NumberFormatException e )
        {
            e.printStackTrace();
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        // Trim the fat
        papers.trimToSize();
        
        return new Author(papers.toArray(new Paper[papers.size()]));
    }
    
    private static LuceneHandler generateAuthorLuceneIndex( DatabaseConnection db, int authorid )
    {
        // Create Field List and get a Lucene Instance
        String[] fieldList = {DBLP.PAPERID, DBLP.PAPERS_YEAR, DBLP.PAPERS_ABSTRACT};
        LuceneHandler luceneHandle = LuceneHandler.createLuceneInstance(LUCENE_DIR);
        
        // Query DB for something
        StringBuilder queryBuffer = new StringBuilder();
        
        // Adding Selects
        queryBuffer.append( "Select ");
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[0] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[1] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[2] + "\n" );
        
        // Adding From
        queryBuffer.append("From ");
        queryBuffer.append(DBLP.WRITTENBY + ", ");
        queryBuffer.append(DBLP.PAPERS + "\n");
        
        // Adding Where
        queryBuffer.append("Where ");
        queryBuffer.append(DBLP.WRITTENBY + "." + DBLP.PAPERID + "=" + DBLP.PAPERS + "." + DBLP.PAPERID + "\n");
        
        // Adding And 
        queryBuffer.append("And ");
        queryBuffer.append(DBLP.WRITTENBY + "." + DBLP.PERSONID + "='" + authorid + "'");
        
        // Populating Lucene Index
        ResultSet rs = db.query(queryBuffer.toString());
        try
        {
            while(rs.next())
            {
                ArrayList<String> currentResult = new ArrayList<String>();
                for ( String field : fieldList )
                {
                    String value = rs.getString(field);
                    value = ( value == null ) ? "" : value;
                    currentResult.add(value);
                }
                
                // Add document with given array of fields and values
                luceneHandle.addDocument(Integer.parseInt(rs.getString(DBLP.PAPERID)), fieldList, currentResult);
            }
            luceneHandle.fixIndexList();
            rs.close();
        } catch ( NumberFormatException e )
        {
            e.printStackTrace();
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return luceneHandle;
    }

    private static LuceneHandler generateSelfCoAuthorLuceneIndex( DatabaseConnection db, int authorid )
    {
        // Start by creating a list of authorids
        ArrayList<Integer> authors = generateAuthorList( db, authorid );
        
        LuceneHandler luceneHandle = generateAuthorListLuceneIndex(db, authors);
        
        return luceneHandle;
    }
    
    private static LuceneHandler generateCoAuthorLuceneIndex( DatabaseConnection db, int authorid )
    {
        // Start by creating a list of authorids
        ArrayList<Integer> authors = generateAuthorList( db, authorid );
        
        // Remove Self from authors
        authors.remove(0);
        
        LuceneHandler luceneHandle = generateAuthorListLuceneIndex(db, authors);
        
        return luceneHandle;
    }

    private static LuceneHandler generateAuthorListLuceneIndex( DatabaseConnection db, ArrayList<Integer> authors )
    {
        // Create Field List and get a Lucene Instance
        String[] fieldList = {DBLP.PAPERID, DBLP.PAPERS_YEAR, DBLP.PAPERS_ABSTRACT};
        LuceneHandler luceneHandle = LuceneHandler.createLuceneInstance(LUCENE_DIR);
        
        // Query DB for something
        StringBuilder queryBuffer = new StringBuilder();
        
        // Adding Selects
        queryBuffer.append( "Select ");
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[0] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[1] + ", " );
        queryBuffer.append(DBLP.PAPERS + "." + fieldList[2] + "\n" );
        
        // Adding From
        queryBuffer.append("From ");
        queryBuffer.append(DBLP.WRITTENBY + ", ");
        queryBuffer.append(DBLP.PAPERS + "\n");
        
        // Adding Where
        queryBuffer.append("Where ");
        queryBuffer.append(DBLP.WRITTENBY + "." + DBLP.PAPERID + "=" + DBLP.PAPERS + "." + DBLP.PAPERID + "\n");
        
        // Adding And ( hacks )
        queryBuffer.append("And ( ");
        for ( int i = 0; i < authors.size(); i++ )
        {
            Integer author = authors.get(i);
            queryBuffer.append(DBLP.WRITTENBY + "." + DBLP.PERSONID + "='" + author + "'");
            
            if( authors.size() - i == 1)
                queryBuffer.append(" )");
            else
                queryBuffer.append(" OR ");
        }
        
        // Populating Lucene index
        ResultSet rs = db.query(queryBuffer.toString());
        try
        {
            while(rs.next())
            {
                ArrayList<String> currentResult = new ArrayList<String>();
                for ( String field : fieldList )
                {
                    String value = rs.getString(field);
                    value = ( value == null ) ? "" : value;
                    currentResult.add(value);
                }
                
                // Add document with given array of fields and values
                luceneHandle.addDocument(Integer.parseInt(rs.getString(DBLP.PAPERID)), fieldList, currentResult);
            }
            rs.close();
            luceneHandle.fixIndexList();
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        return luceneHandle;
    }

    /**
     * Gets the list of co-authors for a given author, including itself.
     * @param db
     * @param authorid
     * @return
     */
    private static ArrayList<Integer> generateAuthorList( DatabaseConnection db, int authorid )
    {
        ArrayList<Integer> authorList = new ArrayList<Integer>();
        authorList.add(authorid);
        
        // Query for the co-authors of authorid
        String queryBuffer = "Select personid2 From coauthors Where personid1 ='" + authorid + "'";
        
        ResultSet rs = db.query(queryBuffer);
        
        try
        {
            while(rs.next())
            {
                authorList.add( Integer.parseInt(rs.getString(1)));
            }
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return authorList;
    }
}

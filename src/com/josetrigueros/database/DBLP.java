package com.josetrigueros.database;

/**
 * Represents the dblp database tables and columns
 * @author Jose Trigueros
 *
 */
public class DBLP
{
    public static final String PATH = "./dblp.sqlite";
    public static final String PAPERID   = "paperid";
    
    public static final String PAPERS = "papers";
    public static final String PAPERS_ABSTRACT  = "abstract";
    public static final String PAPERS_YEAR      = "year";
    
    public static final String WRITTENBY = "writtenby";
    public static final String PERSONID = "personid";
}

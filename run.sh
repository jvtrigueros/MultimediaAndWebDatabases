echo Computing Task 1
while read line
do
    echo Starting $line
    java -jar Phase1.jar print_paper_vector $line tf > ./output/task1_${line}_tf.txt
    java -jar Phase1.jar print_paper_vector $line tfidf > ./output/task1_${line}_tfidf.txt
    echo Completed $line
done < "./input/task1.txt"

echo Computing Task 2
while read line
do
    echo Starting $line
    java -jar Phase1.jar print_author_vector $line tf > ./output/task2_${line}_tf.txt
    java -jar Phase1.jar print_author_vector $line tfidf > ./output/task2_${line}_tfidf.txt
    echo Completed $line
done < "./input/task2_3.txt"

echo Computing Task 3
while read line
do
    echo Starting $line
    java -jar Phase1.jar differentiate_author $line tfidf > ./output/task3_${line}_tfidf2.txt
    java -jar Phase1.jar differentiate_author $line pf > ./output/task3_${line}_pf.txt
    echo Completed $line
done < "./input/task2_3.txt"

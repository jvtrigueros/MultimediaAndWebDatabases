/**
 * 
 */
package com.josetrigueros.entities;

import java.util.ArrayList;
import java.util.HashMap;

import com.josetrigueros.database.DBLP;
import com.josetrigueros.lucene.LuceneHandler;

/**
 * This class represents a body of documents.
 * 
 * @author Jose Trigueros
 *
 */
public class Corpus
{
    private LuceneHandler luceneHandler;
    private HashMap<Integer, Paper> corpusMap;
    private int corpusSize;
    private int maxYear;
    private int minYear;
    private boolean isWeighted;
    
    public Corpus()
    {
        corpusMap = new HashMap<Integer,Paper>();
        maxYear = -1;
        minYear = -1;
        isWeighted = false;
    }
    
    public Corpus(LuceneHandler lucene)
    {
        this();
        luceneHandler = lucene;
        processLuceneIndex();
    }
    
    public Corpus( LuceneHandler lucene, boolean isWeighted )
    {
        this(lucene);
        isWeighted = true;
    }
    
    public int termFreq( Keyword keyword )
    {
        int count = 0;
        for ( Paper paper : corpusMap.values() )
        {
            if( paper.contains(keyword) )
                count++;
        }
        
        return count;
    }
        
    public int inverseTermFreq( Keyword keyword )
    {
        return corpusSize - termFreq(keyword);
    }

    public Paper getPaper( Integer paperid )
    {
        return corpusMap.get(paperid);
    }
    
    public int getCorpusSize()
    {
        return corpusSize;
    }

    private void processLuceneIndex()
    {
        // Store corpus size
        corpusSize =  luceneHandler.getReader().numDocs();
        
        // Temporary list of papers
        ArrayList<Paper> paperList = new ArrayList<Paper>();
        
        for ( Integer paperid : luceneHandler.getIndexList() )
        {
            // Get year and adjust year ranges
            int year = Integer.parseInt(luceneHandler.getFieldValue(paperid, DBLP.PAPERS_YEAR));
            adjustYearRanges( year );
            
            // Create a paper with terms and frequencies
            Paper currentPaper = new Paper( paperid, year , luceneHandler.getTermFrequencies(paperid) );
            paperList.add(currentPaper);
            corpusMap.put(paperid, currentPaper);
        }
        
        // Calculate the idf of each of the papers
        double yearRange = maxYear-minYear;
        for ( Paper paper : paperList )
        {
            Keyword[] keywordList = paper.getKeywordList();
            for ( Keyword keyword :  keywordList )
            {
                int documentFrequency = luceneHandler.getDocumentFrequency(keyword.getKeyword());
                double inverseDocumentFrequency = Math.log10( ((double)corpusSize)/documentFrequency );
                keyword.setInverseDocFreq(inverseDocumentFrequency);
            }
            
            // Set the weight given to the paper based on the year, weight range 0-1.
            if( isWeighted )
                paper.setWeight((paper.getYear()-minYear)/yearRange);
        }
        
        // This may not work, close Lucene index
        luceneHandler.close();
    }

    /**
     * Adjust the range of the years in the Corpus
     * @param newYear incoming candidate to be a min or max year
     */
    private void adjustYearRanges( int newYear )
    {
        // Set the lower end for years
        if( minYear == -1 )
            minYear = newYear;
        else
            minYear = ( minYear > newYear ) ? newYear : minYear;
        
        // Set the higher end for years
        if( maxYear == -1 )
            maxYear = newYear;
        else
            maxYear = ( maxYear < newYear ) ? newYear : maxYear;
    }
}

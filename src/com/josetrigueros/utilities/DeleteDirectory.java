/**
 * 
 */
package com.josetrigueros.utilities;

import java.io.File;

/**
 * Modified source from the author to recursively delete a directory.
 * 
 * @author http://www.mkyong.com/java/how-to-delete-directory-in-java/
 *
 */
public class DeleteDirectory
{
    // Private constructor to prevent instantiation.
    private DeleteDirectory(){}
    
    public static void recursive( String directory )
    {
        File dir = new File(directory);
        
        if ( dir.exists() )
        {
            if ( dir.isDirectory() )
            {
                String[] fileList = dir.list();
                if ( fileList.length == 0 )
                    dir.delete();
                else
                {
                    for ( String file : fileList )
                    {
                        recursive(new File(dir, file).getAbsolutePath());
                    }
                    
                    // Then finally delete the folder after the contents have been deleted
                    if ( dir.isDirectory() )
                        dir.delete();
                }
            }
            else
            {
                dir.delete();
            }
        }
    }
    
    public static void main( String[] args )
    {
        DeleteDirectory.recursive("./TestIndex");
    }
    
}

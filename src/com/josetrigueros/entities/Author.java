package com.josetrigueros.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Author
{
    private Paper[] papers;
    private Paper averagePaper;
        
    public Author(Paper[] papers)
    {
        this.papers = papers;
        processPapers();
    }

    private void processPapers()
    {
        averagePaper = new Paper(-1, -1, null);
        
        for ( Paper paper : papers )
        {
            averagePaper = averagePaper.add(paper);
        }
        
        averagePaper = averagePaper.divideScalar(papers.length);
    }

    /**
     * @return the averagePaper
     */
    public Paper getAveragePaper()
    {
        return averagePaper;
    }

    public void printTFIDF2( Author self_coauthor )
    {
        Keyword[] termList = averagePaper.getKeywordList();
        ArrayList<Keyword> otherTermList = new ArrayList<Keyword>(Arrays.asList(self_coauthor.getAveragePaper().getKeywordList()));
        Collections.sort(otherTermList);
        for ( Keyword keyword : termList )
        {
            int index = Collections.binarySearch(otherTermList, keyword);
            if ( index >= 0 )
            {
                keyword.setInverseDocFreq( otherTermList.get(index).getInverseDocFreq());
            }
        }
        averagePaper.printTFIDF();
    }
}

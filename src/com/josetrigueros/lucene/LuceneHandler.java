/**
 * 
 */
package com.josetrigueros.lucene;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.TermVector;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermFreqVector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import com.josetrigueros.database.DBLP;
import com.josetrigueros.entities.Keyword;

/**
 * A class used as a proxy for the Lucene API.
 * 
 * @author Jose Trigueros
 *
 */
public class LuceneHandler
{
    private StandardAnalyzer analyzer;
    private Directory index;
    private IndexReader reader;
    private IndexWriter writer;
    private ArrayList<Integer> indexList;
    
    private final static Version VERSION = Version.LUCENE_36;
    private final static String STOP_WORDS_PATH = "./english.stop";
    
    /**
     * Private constructor to prevent instantiation.
     * @param reader TODO
     * @param writer TODO
     */
    private LuceneHandler(StandardAnalyzer analyzer, Directory index, IndexReader reader, IndexWriter writer )
    {
        this.analyzer = analyzer;
        this.index = index;
        this.reader = reader;
        this.writer = writer;
        indexList = new ArrayList<Integer>();
    }

    public void addDocument( int indexID, String[] fieldList, ArrayList<String> valueList )
    {
        try
        {
            // Create index writer and determine if this should be analyzed or not
//            writer = new IndexWriter(index, new IndexWriterConfig(VERSION, analyzer));
            
            // Create a new document
            Document doc = new Document();
            
            // Loop through the field and value pairs, and add them to the document
            for ( int i = 0; i < fieldList.length; i++ )
            {
                String field = fieldList[i]; 
                String value = valueList.get(i);
                
                // Create a new field for the current pair and add it to the document
                doc.add( new Field(field,value,Field.Store.YES,Field.Index.ANALYZED,TermVector.YES) );
            }

            // Add the document to the index
            writer.addDocument(doc);
            writer.commit();
            
            // Close writer
//            writer.close();
            
        } catch ( CorruptIndexException e )
        {
            e.printStackTrace();
        } catch ( LockObtainFailedException e )
        {
            e.printStackTrace();
        } catch ( IOException e )
        {
            e.printStackTrace();
        }
        
        // If everything was kosher, add the unique id to the index list
        indexList.add(indexID);
    }

    /**
     * This should be run after adding documents to the index to ensure that the index list is in sync with Lucene 
     */
    public void fixIndexList()
    {
        // Get the number of documents before clearing it
        int numDocs = indexList.size();
        
        // Make sure list is clean
        indexList.clear();
        
        for ( int i = 0; i < numDocs; i++ )
        {
            // Get the paper id of the corresponding paper
            String fieldValue;
            try
            {
                fieldValue = getReader().document(i).getFieldable(DBLP.PAPERID).stringValue();
                indexList.add(Integer.parseInt(fieldValue));
            } catch ( CorruptIndexException e )
            {
                e.printStackTrace();
            } catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Extracts a list of terms and their respective frequencies from a paper 
     * @param indexID the id of the paper 
     * @return a list of terms
     */
    public Keyword[] getTermFrequencies(Integer indexID)
    {
        ArrayList<Keyword> keywordList = new ArrayList<Keyword>();
        
        // Get documents respective Lucene index
        int luceneIndex = indexList.indexOf(indexID);
        
        if( luceneIndex != -1)
        {
            try
            {
                // Get Frequency Vector, this is kind of cheating since I know which field I need, non-dynamic
                TermFreqVector freqVector = reader.getTermFreqVector(luceneIndex, DBLP.PAPERS_ABSTRACT);
                
                // Frequency Vector might be null
                if( freqVector == null )
                    return new Keyword[]{};
                
                
                // Start churning through the terms and frequencies
                double totalTermCount = 0.0;
                for ( int i = 0; i < freqVector.size(); i++ )
                {
                    Keyword word = new Keyword(freqVector.getTerms()[i]);
                    int freq = freqVector.getTermFrequencies()[i];
                    word.setFreq(freq);
                    keywordList.add(word);
                    
                    // Start accumulating the sum of all the terms
                    totalTermCount += freq;
                }
                
                // Normalize the frequency
                for ( Keyword keyword : keywordList )
                {
                    keyword.setFreq(keyword.getFreq()/totalTermCount);
                }
            } catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
        else
            System.out.println("Index does not contain such id: " + indexID);
        
        // Trim some of that excess fat
        keywordList.trimToSize();
        
        return keywordList.toArray(new Keyword[keywordList.size()]);
    }
    
    public int getDocumentFrequency( String term )
    {
        int documentFreq = 0;
        try
        {
            documentFreq = reader.docFreq( new Term(DBLP.PAPERS_ABSTRACT, term));
        } catch ( IOException e )
        {
            e.printStackTrace();
        }
        
        return documentFreq;
    }
    
    public String getFieldValue( Integer indexID, String field )
    {
        String fieldValue = "";
        
        // Get documents respective Lucene index
        int luceneIndex = indexList.indexOf(indexID);
        
        if( luceneIndex != -1)
        {
            try
            {
                // TODO If you encounter problems here change reader to getReader()
                fieldValue = reader.document(luceneIndex).getFieldable(field).stringValue();
            } catch ( CorruptIndexException e )
            {
                e.printStackTrace();
            } catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
        return fieldValue;
    }

    /**
     * Release any directory that this Lucene instance is holding.
     */
    public void close()
    {
        try
        {
            if ( IndexWriter.isLocked(index))
                writer.close();
            analyzer.close();
            index.close();
            reader.close();
//                IndexWriter.unlock(index);
        } catch ( CorruptIndexException e )
        {
            e.printStackTrace();
        } catch ( IOException e )
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("resource")
    public static LuceneHandler createLuceneInstance(String indexDir)
    {
        Reader stopWords; 
        StandardAnalyzer analyzer = null;
        Directory index = null;
        IndexReader reader = null;
        IndexWriter writer = null;
        
        try
        {
            stopWords = new FileReader( new File(STOP_WORDS_PATH) );
            analyzer = new StandardAnalyzer(VERSION, stopWords);
            
            // If the argument given is null, then create a RAMDirecotry
            if( indexDir == null )
            {
                index = new RAMDirectory();
            }
            // Otherwise, check if the directory exist act accordingly.
            else
            {
                File indexFile = new File(indexDir);
                if( indexFile.exists() )
                    index = SimpleFSDirectory.open(indexFile);
                else
                {
                    index = new SimpleFSDirectory(indexFile);
                }
            }
            
            writer = new IndexWriter(index, new IndexWriterConfig(VERSION, analyzer));
            
            // Check if it's possible to clear the index here.
            writer.commit();
            reader = IndexReader.open(index);
            
        } catch ( IOException e )
        {
            e.printStackTrace();
        }
        
        return new LuceneHandler(analyzer, index, reader, writer);
    }

    /**
     * @return the analyzer
     */
    public StandardAnalyzer getAnalyzer()
    {
        return analyzer;
    }

    /**
     * @param analyzer the analyzer to set
     */
    public void setAnalyzer( StandardAnalyzer analyzer )
    {
        this.analyzer = analyzer;
    }

    /**
     * @return the index
     */
    public Directory getIndex()
    {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex( Directory index )
    {
        this.index = index;
    }

    /**
     * @return the reader
     */
    public IndexReader getReader()
    {
        IndexReader newReader;
        try
        {
            newReader = IndexReader.openIfChanged(reader);
            if( newReader != null )
            {
                reader.close();
                reader = newReader;
            }
        } catch ( IOException e )
        {
            e.printStackTrace();
        }
        return reader;
    }

    /**
     * @param reader the reader to set
     */
    public void setReader( IndexReader reader )
    {
        this.reader = reader;
    }

    /**
     * @return the indexList
     */
    public ArrayList<Integer> getIndexList()
    {
        return indexList;
    }

    /**
     * @param indexList the indexList to set
     */
    public void setIndexList( ArrayList<Integer> indexList )
    {
        this.indexList = indexList;
    }
}

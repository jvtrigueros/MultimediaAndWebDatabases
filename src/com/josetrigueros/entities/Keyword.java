/**
 * 
 */
package com.josetrigueros.entities;

import java.util.Comparator;


/**
 * Holds a term and properties associated with it.
 * 
 * @author Jose Trigueros
 *
 */
public class Keyword implements Comparable<Keyword>
{
    private String keyword;
    private double freq;
    private double inverseDocFreq;
    
    public static Comparator<Keyword> TFIDFComparator = new Comparator<Keyword>() {
        
        @Override
        public int compare( Keyword o1, Keyword o2 )
        {
            int output = 0;
            if( o1.getTfidf() < o2.getTfidf() )
                output = 1;
            else if ( o1.getTfidf() > o2.getTfidf() )
                output = -1;
            return output;
        }
    };

    public static Comparator<Keyword> TFComparator = new Comparator<Keyword>() {
        
        @Override
        public int compare( Keyword o1, Keyword o2 )
        {
            int output = 0;
            if( o1.getFreq() < o2.getFreq() )
                output = 1;
            else if ( o1.getFreq() > o2.getFreq() )
                output = -1;
            return output;
        }
    };

    public Keyword( String keyword )
    {
        this.keyword = keyword;
        freq = 0.0;
        inverseDocFreq = 0.0;
    }

    public Keyword multiplyScalar( double weight )
    {
        Keyword k = new Keyword(keyword);
        k.setFreq(freq*weight);
        k.setInverseDocFreq(inverseDocFreq);
        return k;
    }

    /**
     * @return the freq
     */
    public double getFreq()
    {
        return freq;
    }

    /**
     * @param freq the freq to set
     */
    public void setFreq( double freq )
    {
        this.freq = freq;
    }

    /**
     * @return the inverseDocFreq
     */
    public double getInverseDocFreq()
    {
        return inverseDocFreq;
    }

    /**
     * @param inverseDocFreq the inverseDocFreq to set
     */
    public void setInverseDocFreq( double inverseDocFreq )
    {
        this.inverseDocFreq = inverseDocFreq;
    }

    /**
     * @return the tfidf
     */
    public double getTfidf()
    {
        return freq*inverseDocFreq;
    }

    /**
     * @return the term
     */
    public String getKeyword()
    {
        return keyword;
    }
    
    @Override
    public String toString()
    {
        return "Term: " + keyword;
    }

    @Override
    public int compareTo( Keyword o )
    {
        return keyword.compareTo(o.getKeyword());
    }
}

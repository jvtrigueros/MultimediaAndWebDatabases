/**
 * 
 */
package com.josetrigueros.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * A wrapper class used to hide the connection details between a client and a database.
 * @author Jose Trigueros
 */
public class DatabaseConnection
{
    private final String DB_URL = "jdbc:sqlite:";
    private final String DB_DRIVER = "org.sqlite.JDBC";
    
    private String username;
    private String password;
    private String databaseName;
    private Connection connection;
    
    /**
     * Establish a connection with the database.
     * @param username a username that has access to the database
     * @param password the corresponding password for <b>username</b>
     * @param databaseName the name of the database to connect to
     */
    public DatabaseConnection(String username, String password, String databaseName)
    {
        this.username = username;
        this.password = password;
        this.databaseName = databaseName;
        connect();
    }
    
    /**
     * Constructor taking in database name
     * @param databasePath Path to the sqlite database
     */
    public DatabaseConnection(String databasePath)
    {
        this("","",databasePath);
    }
    
    /**
     * Attempts to create a connection to the database. 
     * @return true if connection was successful, false otherwise.
     */
    private boolean connect()
    {
        try
        {
            if( connection == null )
            {
                // Load JDBC using the current class loader
                Class.forName(DB_DRIVER);
                connection = DriverManager.getConnection(DB_URL+databaseName,username,password);
            }
            
        } catch ( SQLException e )
        {
            e.printStackTrace();
        } catch ( ClassNotFoundException e )
        {
            e.printStackTrace();
        }
        
        return (connection == null) ? false : true;
    }
    
    /**
     * Closes the connection if active.
     */
    public void disconnect()
    {
        if( connection != null )
        {
            try
            {
                connection.close();
                connection = null;
            } catch ( SQLException e )
            {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Performs a static SQL query, i.e. <code>SELECT x FROM y</code>
     * @param query static SQL query
     * @return the result of the query
     */
    public ResultSet query(String query)
    {
        ResultSet rs = null;
        try
        {
            rs = connection.createStatement().executeQuery(query);
        } catch ( SQLException e )
        {
            e.printStackTrace();
        }
        return rs;
    }

    
    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName( String databaseName )
    {
        this.databaseName = databaseName;
    }

    
    /**
     * @return the connection
     */
    public Connection getConnection()
    {
        return connection;
    }
}
